class ChangeColumnType < ActiveRecord::Migration[5.0]
  def change
      change_column :student_answer_logs, :question_id, 'integer USING CAST(question_id AS integer)'
  end
end
