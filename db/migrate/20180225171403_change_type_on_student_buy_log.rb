class ChangeTypeOnStudentBuyLog < ActiveRecord::Migration[5.0]
  def change
    change_column :student_buy_logs, :paper_set_id, 'integer USING CAST(paper_set_id AS integer)'
  end
end
