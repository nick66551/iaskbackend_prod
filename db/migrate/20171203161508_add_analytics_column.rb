class AddAnalyticsColumn < ActiveRecord::Migration[5.0]
  def change
    add_column :student_correct_rates, :finished, :boolean
    add_column :student_correct_rates, :finished_timestamp, :datetime
    change_column :questions, :answer_count, :integer, :default => 0
  end
end
