class ChangeTypeOnStudentPaperLog < ActiveRecord::Migration[5.0]
  def change
    change_column :student_paper_logs, :paper_id, 'integer USING CAST(paper_id AS integer)'
  end
end
