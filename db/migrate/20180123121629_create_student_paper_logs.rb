class CreateStudentPaperLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :student_paper_logs do |t|
      t.string :student_id
      t.string :string
      t.string :paper_id
      t.float :correct_rate
      t.float :finished_rate
      t.datetime :answer_time
      t.integer :answer_times

      t.timestamps
    end
  end
end
