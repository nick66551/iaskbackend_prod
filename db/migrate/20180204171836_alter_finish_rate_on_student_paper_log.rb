class AlterFinishRateOnStudentPaperLog < ActiveRecord::Migration[5.0]
  def change
    rename_column :student_paper_logs, :finished_rate, :finish_rate
  end
end
