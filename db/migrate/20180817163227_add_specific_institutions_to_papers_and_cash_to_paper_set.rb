class AddSpecificInstitutionsToPapersAndCashToPaperSet < ActiveRecord::Migration[5.0]
  def change
    add_column :papers,:specific_institution,:string
    add_column :papers,:specific_institution_visible,:boolean
    add_column :paper_sets,:cash,:integer
  end
end
