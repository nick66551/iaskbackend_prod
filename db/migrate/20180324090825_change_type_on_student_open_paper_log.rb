class ChangeTypeOnStudentOpenPaperLog < ActiveRecord::Migration[5.0]
  def change
    change_column :student_open_paper_logs, :paper_id, 'integer USING CAST(paper_id AS integer)'
  end
end
