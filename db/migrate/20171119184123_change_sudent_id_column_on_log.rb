class ChangeSudentIdColumnOnLog < ActiveRecord::Migration[5.0]
  def change
    change_column :student_answer_logs, :student_id, :string
  end
end
