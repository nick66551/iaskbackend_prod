json.extract! paper_source, :id, :name, :platform_type, :created_at, :updated_at
json.url paper_source_url(paper_source, format: :json)
