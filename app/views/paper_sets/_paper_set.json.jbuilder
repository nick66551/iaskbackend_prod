json.extract! paper_set, :id, :title, :price, :public_date, :description, :active, :platform_type, :created_at, :updated_at
json.url paper_set_url(paper_set, format: :json)
